package fandy.santoso.sinarmas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SinarmasApplication {
    public static void main(String[] args) {
        SpringApplication.run(SinarmasApplication.class, args);
    }

}
