package fandy.santoso.sinarmas.controller;

import fandy.santoso.sinarmas.entity.User;
import fandy.santoso.sinarmas.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class PhoneController {
    @Autowired
    UserRepository userRepository;

    @GetMapping({"/", ""})
    public ResponseEntity<List<User>> getUsers() {
        List<User> users = new ArrayList<>();
        try {
            users = userRepository.findAll();
            if (users.isEmpty())
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            else
                return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping({"/", ""})
    public ResponseEntity<User> saveUsers(@RequestBody User user) {
        try {
            user = userRepository.save(user);
            return new ResponseEntity<>(user, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping({"/", ""})
    public ResponseEntity<User> updateUsers(@RequestBody User user) {
        try {
            user = userRepository.save(user);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping({"/", ""})
    public ResponseEntity<HttpStatus> deleteUsers(@RequestBody User user) {
        try {
            userRepository.delete(user);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/name")
    public ResponseEntity<List<User>> getUsersByName(@RequestParam(value = "name") String name) {
        try {
            List<User> users = userRepository.findByNameContaining(name);
            if (users.isEmpty())
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            else
                return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id")
    public ResponseEntity<User> getUsersById(@RequestParam(value = "id") Long id) {
        try {
            User user = userRepository.findById(id).orElse(null);
            if (null == user)
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            else
                return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/id")
    public ResponseEntity<HttpStatus> deleteUsersById(@RequestParam(value = "id") Long id) {
        try {
            User user = userRepository.findById(id).orElse(null);
            if (null == user) {
            }
            else{
                userRepository.delete(user);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/phone")
    public ResponseEntity<List<User>> getUsersByPhone(@RequestParam(value = "phoneNumber") String phoneNumber) {
        try {
            List<User> users = userRepository.findByPrimaryPhoneNumberContaining(phoneNumber);
            if (users.isEmpty())
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            else
                return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
